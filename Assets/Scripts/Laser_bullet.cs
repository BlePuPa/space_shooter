﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser_bullet : MonoBehaviour
{
    // Start is called before the first frame update
    
        public float velocity;
        public Vector2 direction;


    // Update is called once per frame
    void Update()
    {
              
              transform.Translate(direction * velocity *Time.deltaTime); 

        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            Destroy(gameObject);
        }
    }
               
}
