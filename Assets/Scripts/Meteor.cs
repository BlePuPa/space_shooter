﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    Vector2 Speed;
    public GameObject[] meteorito;
    int Seleccionado;
    public AudioSource audioSource;
    public ParticleSystem ps;

    public GameObject meteorToInstantiate;

    void Awake()
    {
        for (int i = 0; i < meteorito.Length; i++)
        {
            meteorito[i].SetActive(false);
        }

        Seleccionado = Random.Range(0, meteorito.Length);
        meteorito[Seleccionado].SetActive(true);

        Speed.x = Random.Range(-5, 0);
        Speed.y = Random.Range(-4, 5);
    }
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Speed * Time.deltaTime);
        meteorito[Seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish")
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Bullet")
        {
            StartCoroutine(DestroyMeteor());
        }
    }
    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        meteorito[Seleccionado].gameObject.SetActive(false);

        Destroy(GetComponent<BoxCollider2D>());

        //Particula Lanzada
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);
    }
    public virtual void InstanceMeteors(object Identity)
    {
        Instantiate(meteorToInstantiate, this, transform.position, Quaternion; Identity,null);
        Instantiate(meteorToInstantiate, this, transform.position, Quaternion; Identity,null);
        Instantiate(meteorToInstantiate, this, transform.position, Quaternion; Identity,null);
        Instantiate(meteorToInstantiate, this, transform.position, Quaternion; Identity,null);
    }
}