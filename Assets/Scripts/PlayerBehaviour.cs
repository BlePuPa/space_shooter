﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    private Vector2 shipAxis;
    public Vector2 Limits;
    public float speed;

    public float shootTime = 0;

    public Weapon weapon;

    public Propeller prop;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    public int lives = 3;
    private bool iamDead = false;

    void Update()
    {
        transform.Translate(shipAxis * speed * Time.deltaTime);

        //Limites de la nave
        if (transform.position.y > Limits.y) //Limite Arriba
        {
            transform.position = new Vector3(transform.position.x, Limits.y, transform.position.z);
        }

        if (transform.position.y < -Limits.y) //Limite Abajo
        {
            transform.position = new Vector3(transform.position.x, -Limits.y, transform.position.z);
        }

        if (transform.position.x > Limits.x) //Limite derecha
        {
            transform.position = new Vector3(Limits.x, transform.position.y, transform.position.z);
        }

        if (transform.position.x < -Limits.x) //Limite Izquierda
        {
            transform.position = new Vector3(-Limits.x, transform.position.y, transform.position.z);
        }

        if(shipAxis.x>0){
            prop.BlueFire();
        }else{
            prop.Stop();
        }

    }

    public void SetAxis(Vector2 axis) {
            shipAxis = axis;
    }
           
    

        
        public void Shoot() {
            if (shootTime>weapon.GetCadencia()){
                 shootTime = 0.0f;
                 weapon.Shoot();
            }             
            Debug.Log("Pium pium");
            weapon.Shoot();
        }

    public void OnTriggerEnter2D(Collider2D other)
    {
    if(other.tag == "Meteor")
        {
            StartCoroutine(DestroyShip());

        }    
    }


    IEnumerator DestroyShip()
    {
        //Indico que estoy muerto
        iamDead = true;

        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Desactivo el propeller
        prop.gameObject.SetActive(false);

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Miro si tengo mas vidas
        if (lives > 0)
        {
            //Vuelvo a activar el jugador
            iamDead = false;
            graphics.SetActive(true);
            collider.enabled = true;
            //Activo el propeller
            prop.gameObject.SetActive(true);
        }
    }

IEnumerator inMortal()
    {
        iamDead = false;
        graphics.SetActive(true);
        prop.gameObject.SetActive(true);

        for(int i = 0; i < 15; i++)
        {
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        //activo el collider        
        collider.enabled = true;
    
    }




}

