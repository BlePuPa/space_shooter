﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour

{
    private Vector2 speed = new Vector2(0,0);

    private void Start()
    {
        StartCoroutine(enemyBehaviour());
    }

    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }
    IEnumerator enemyBehaviour()
    {
        while (true)
        {
            //Avanza
            speed.x = -1f;
            speed.y = -1f;

            yield return new WaitForSeconds(1.0f);
            //Se para
            speed.x = 0f;
            speed.y = 0f;

            yield return new WaitForSeconds(1.0f);
            speed.x = -1f;
            speed.y = 1f;

            yield return new WaitForSeconds(1.0f);
            //Dispara

           GameObject bala = Instantiate(bullet, transform.position, Quaternion.identity, null);
            bala.transform.Rotate(0, 0, -180);
        }
    }
}
